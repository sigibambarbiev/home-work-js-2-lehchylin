//  1. Які типи даних є в Javascript?

//     Есть восемь типов данных:
//    - число (Number);
//    - строка (String);
//    - булевый тип (логический) (true/false);
//    - null (специальное значение "нуль, ничто");
//    - undefined (специальное значение, "ничего нет, значение не определено");
//    - BigInt (большое число);
//    - символ;
//    - объект;

// 2. У чому різниця між == і ===?

//     Это операторы сравнения, тип результата сравнения булевый (true/false).
//     ==  нестрогое сравнение, если сравниваемые значения имеют разный тип, то они
//     неявно приводятся  к одному типу и сравниваются.
//     === строгое сравнение, сравниваются не только значения, но и тип данных значений. Если
//     сравниваются значения с разными типами данных, то результат false.

// 3. Що таке оператор?.

//    Операторы в JavaScript это определеенные команды, функции, выполняющие какие-то действия и возвращающие результат. Например, = оператор присваивания, (+ - * /) арифметические операторы, (< > <= >=) операторы сравнения.

let nameUser = prompt('Введіть Ваше ім"я', 'Bob');
let ageUser = prompt('Введіть Ваш вік', '18');

while (!isNaN(+nameUser)) {
    nameUser = prompt('Введіть Ваше ім"я', nameUser);
}
const welcomeUser = ('Welcome, ' + nameUser + '!');

while (isNaN(+ageUser)) {
    ageUser = prompt('Введіть Ваш вік', ageUser);
}

if (ageUser < 18) {
    alert('You are not allowed to visit this website.');
} else if ((ageUser >= 18) && (ageUser <= 22)) {
    const middlAgeUser = confirm('Are you sure you want to continue?');

    if (middlAgeUser===true) {
        alert(welcomeUser);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(welcomeUser);
}
